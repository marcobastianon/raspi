#!/bin/bash

# Packages to remove
app="
bluej
greenfoot
netsurf-*
epiphany-browser
wolfram-*
node*
scratch
sonic-pi
claws-mail
minecraft-pi
sense-emu-tools
geany
libreoffice-*
squeak-*
"

sudo apt-get -y remove --purge $app

# Remove automatically installed dependency packages
# sudo apt-get -y autoremove

# Remove some folder and shortcut
folder="
/home/pi/Pictures/
/home/pi/Videos/
/home/pi/Music/
/home/pi/Public/
/home/pi/Templates/
/home/pi/Documents/
/home/pi/python_games/
/usr/share/raspi-ui-overrides/applications/wolfram-language.desktop
/usr/share/raspi-ui-overrides/applications/wolfram-mathematica.desktop
/usr/share/raspi-ui-overrides/applications/magpi.desktop
/usr/share/raspi-ui-overrides/applications/raspi_resources.desktop
/usr/share/raspi-ui-overrides/applications/wpa_gui.desktop
/usr/share/raspi-ui-overrides/applications/epiphany-browser.desktop
/usr/share/raspi-ui-overrides/applications/epiphany-newtab.desktop
/usr/share/raspi-ui-overrides/applications/geany.desktop
/usr/share/raspi-ui-overrides/applications/netsurf-gtk.desktop
/usr/share/raspi-ui-overrides/applications/bluej.desktop
/usr/share/raspi-ui-overrides/applications/evince.desktop
/usr/share/raspi-ui-overrides/applications/greenfoot.desktop
/usr/share/raspi-ui-overrides/applications/libreoffice-startcenter.desktop
/usr/share/raspi-ui-overrides/applications/libreoffice-draw.desktop
/usr/share/raspi-ui-overrides/applications/libreoffice-math.desktop
/usr/share/raspi-ui-overrides/applications/minecraft-pi.desktop
/usr/share/raspi-ui-overrides/applications/mono-runtime-common.desktop
/usr/share/raspi-ui-overrides/applications/mono-runtime-terminal.desktop
/usr/share/raspi-ui-overrides/applications/openjdk-7-policytool.desktop
/usr/share/raspi-ui-overrides/applications/pistore.desktop
/usr/share/raspi-ui-overrides/applications/hplj1020.desktop
/usr/share/raspi-ui-overrides/applications/scratch.desktop
/usr/share/raspi-ui-overrides/applications/squeak.desktop
/usr/share/raspi-ui-overrides/applications/SuperColliderIDE.desktop
/usr/share/raspi-ui-overrides/applications/idle-python3.2.desktop
/usr/share/raspi-ui-overrides/applications/epiphany.desktop
/usr/share/applications/debian-reference-common.desktop
/usr/share/applications/epiphany-browser.desktop
/usr/share/applications/epiphany-newtab.desktop
"

# Remove Folder
sudo rm -rf $folder

# Install Arduino IDE
# wget "https://downloads.arduino.cc/arduino-1.8.1-linuxarm.tar.xz"
# tar fx arduino-1.8.1-linuxarm.tar.xz
# cd arduino-1.8.1/
# sudo sh install.sh
# cd ..
# rm -rf arduino-1.8.1*

# Install Node.js 6.x
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

# Some Update
sudo apt-get -y update
sudo apt-get -y full-upgrade

# Remove Installation File

cd ..
rm -rf raspi/

echo "- - - "
echo " - - "
echo "All done.. please reboot!"

# End
